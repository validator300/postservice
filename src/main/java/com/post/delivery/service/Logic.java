package com.post.delivery.service;

import com.post.delivery.models.Message;
import com.post.delivery.models.Status;
import com.post.delivery.models.WayBill;
import com.post.delivery.service.MessageService.MessageServiceImpl;
import com.post.delivery.service.WayBillService.WayBillServiceImpl;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.List;
import java.util.Random;

@Service
@EnableAsync
public class Logic {
    WayBillServiceImpl wayBill;
    MessageServiceImpl message;

    @Autowired
    public Logic(WayBillServiceImpl wayBill, MessageServiceImpl message) {
        this.wayBill = wayBill;
        this.message = message;
    }

    @Scheduled(fixedRate = 5000)
    @Async
    @Transactional
    public void applicationStatusProcessing() {
        List<WayBill> bidList = wayBill.packagesWithStatusNew();
        for (int i = 0; i < bidList.size(); i++) {
            long id = bidList.get(i).getId();
            if (message.getMessageForIdWayBill(id).getStatus().equals(Status.NEW.name())) {
                LocalTime timeGoods = LocalTime.from(wayBill.getWayBillForId(id).getCreationWayBill());
                LocalTime newTime = timeGoods.plusSeconds(5);
                compareOfSendingAndReceivingTimes(LocalTime.now(), newTime, id);
                wayBill.updateStatus(bidList.get(i));
            }
        }
    }

    private void compareOfSendingAndReceivingTimes(LocalTime now, LocalTime newTimePlusFive, long item) {
        WayBill wayBill1 = wayBill.getWayBillForId(item);
        if (now.isBefore(newTimePlusFive)) {
            if (packageStatus()) {
                wayBill1.setStatus(Status.DELIVERED.name());
                message.saveOrUpdateNotification(
                        new Message(item, "Received", Status.DELIVERED.name()));
            } else {
                wayBill1.setStatus(Status.EXPIRED.name());
                message.saveOrUpdateNotification(
                        new Message(item, "Overdue", Status.EXPIRED.name()));
            }
        } else {
            wayBill1.setStatus(Status.EXPIRED.name());
            message.saveOrUpdateNotification(
                    new Message(item, "Overdue", Status.EXPIRED.name()));
        }
        wayBill.updateStatus(wayBill1);
    }

    private boolean packageStatus() {
        Random random = new Random();
        return random.nextBoolean();
    }
}
