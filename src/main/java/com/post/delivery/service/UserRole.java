package com.post.delivery.service;

public enum UserRole {
    ADMIN,
    USER
}
