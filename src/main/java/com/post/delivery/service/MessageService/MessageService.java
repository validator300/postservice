package com.post.delivery.service.MessageService;

import com.post.delivery.models.Message;

import java.util.List;

public interface MessageService {
    Message getMessageForIdWayBill(Long id);
    void saveOrUpdateNotification(Message T);
}
