package com.post.delivery.service.MessageService;

import com.post.delivery.models.Message;
import com.post.delivery.repo.RepositoryMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService{

    private final RepositoryMessage repositoryMessage;

    @Autowired
    public MessageServiceImpl(RepositoryMessage repositoryMessage) {
        this.repositoryMessage = repositoryMessage;
    }

    @Override
    public Message getMessageForIdWayBill(Long id) {
        return repositoryMessage.getReferenceById(id);
    }

    @Override
    public void saveOrUpdateNotification(Message T) {
        repositoryMessage.saveAndFlush(T);
    }
}
