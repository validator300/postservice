package com.post.delivery.service.UserService;

import com.post.delivery.models.User;
import com.post.delivery.repo.RepositoryUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    private final RepositoryUser repositoryUser;

    @Autowired
    public UserServiceImpl(RepositoryUser repositoryUser) {
        this.repositoryUser = repositoryUser;
    }

    @Override
    public List<User> getAllUsers() {
        return repositoryUser.findAll();
    }

    @Override
    public User getUserForId(Long id) {
        return repositoryUser.getReferenceById(id);
    }

    @Override
    public User saveUser(User user) {
        return repositoryUser.save(user);
    }

    @Override
    public void deleteUserForId(Long id) {
        User user = getUserForId(id);
        repositoryUser.delete(user);
    }
}
