package com.post.delivery.service.UserService;

import com.post.delivery.models.User;

import java.util.List;

public interface UserService {

    List<User> getAllUsers();
    User getUserForId(Long id);
    User saveUser(User user);
    void deleteUserForId(Long id);
}
