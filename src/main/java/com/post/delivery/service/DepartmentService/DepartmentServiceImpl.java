package com.post.delivery.service.DepartmentService;

import com.post.delivery.models.Department;
import com.post.delivery.repo.RepositoryDepartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService{

    private final RepositoryDepartment repositoryDepartment;

    @Autowired
    public DepartmentServiceImpl(RepositoryDepartment repositoryDepartment) {
        this.repositoryDepartment = repositoryDepartment;
    }

    @Override
    public List<Department> getAllDepartments() {
        return repositoryDepartment.findAll();
    }

    @Override
    public Department getDepartForId(Long id) {
        return repositoryDepartment.getReferenceById(id);
    }

    @Override
    public void save(Department department) {
        repositoryDepartment.save(department);
    }

    @Override
    public void deleteById(Long id) {
        repositoryDepartment.deleteById(id);
    }
}
