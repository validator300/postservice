package com.post.delivery.service.DepartmentService;

import com.post.delivery.models.Department;

import java.util.List;

public interface DepartmentService {
    List<Department> getAllDepartments();
    Department getDepartForId(Long id);
    void save(Department department);
    void deleteById(Long id);
}
