package com.post.delivery.service.WayBillService;

import com.post.delivery.models.Status;
import com.post.delivery.models.WayBill;
import com.post.delivery.repo.RepositoryWayBill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class WayBillServiceImpl implements WayBillService{

    private final RepositoryWayBill repositoryWayBill;

    @Autowired
    public WayBillServiceImpl(RepositoryWayBill repositoryWayBill) {
        this.repositoryWayBill = repositoryWayBill;
    }

    @Override
    public List<WayBill> getAllWayBill() {
        return repositoryWayBill.findAll();
    }

    @Override
    public WayBill getWayBillForId(Long id) {
        return repositoryWayBill.getReferenceById(id);
    }

    @Override
    public void saveWayBill(WayBill wayBill) {
        wayBill.setStatus(Status.NEW.name());
        wayBill.setCreationWayBill(LocalDateTime.now());
        wayBill.setUpdateWayBill(LocalDateTime.now());
        repositoryWayBill.saveAndFlush(wayBill);
    }

    @Override
    public void deleteWayBillForId(Long id) {
        repositoryWayBill.deleteById(id);
    }

    @Override
    public void updateStatus(WayBill wayBill) {
        wayBill.setUpdateWayBill(LocalDateTime.now());
        repositoryWayBill.save(wayBill);
    }

    @Override
    public List<WayBill> packagesWithStatusNew() {
        return repositoryWayBill.packagesWithStatusNew();
    }
}
