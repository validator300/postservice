package com.post.delivery.service.WayBillService;

import com.post.delivery.models.WayBill;
import java.util.List;

public interface WayBillService {
    List<WayBill> getAllWayBill();
    WayBill getWayBillForId(Long id);
    void saveWayBill(WayBill wayBill);
    void deleteWayBillForId(Long id);
    void updateStatus(WayBill wayBill);
    List<WayBill> packagesWithStatusNew();
}
