package com.post.delivery.controllers;

import com.post.delivery.models.Message;
import com.post.delivery.repo.RepositoryMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mess")
@Slf4j
public class MessageController {
    private final RepositoryMessage repositoryMessage;

    @Autowired
    public MessageController(RepositoryMessage repositoryMessage) {
        this.repositoryMessage = repositoryMessage;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getMessageForIdWayBill(@PathVariable Long id){
        log.info("find message for id");
        Message message = repositoryMessage.findById(id).orElse(null);
        if(message == null){
            log.info("find message - null: " + ResponseEntity.notFound().build());
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(message);
    }
}
