package com.post.delivery.controllers;

import com.post.delivery.exception.EmployeeNotFoundException;
import com.post.delivery.exception.InternalServerException;
import com.post.delivery.models.Department;
import com.post.delivery.service.DepartmentService.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/depart")
@Slf4j
public class DepartmentController {

    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/allDeparts")
    public ResponseEntity<List<Department>> getAllDepartments() {
        log.info("find all departs method");
        List<Department> employees = departmentService.getAllDepartments();
        return ResponseEntity.ok(employees);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Department> getDepartForId(@PathVariable Long id) {
        log.info("get department for id method");
        try {
            Department employee = departmentService.getDepartForId(id);
            return ResponseEntity.ok(employee);
        }
        catch (EmployeeNotFoundException ex){
            log.info("not found department:" + ResponseEntity.notFound().build());
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/save")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Department> saveDepartment(@RequestBody Department depart) {
        log.info("save department method");
        try{
            departmentService.save(depart);
            Long id = depart.getId();
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();
            return ResponseEntity.created(location).build();
        } catch (InternalServerException ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteDepartmentForId(@PathVariable Long id) {
        log.info("delete department method");
        try{
            departmentService.deleteById(id);
            return ResponseEntity.ok().build();
        }catch (EmployeeNotFoundException ex){
            log.info("not found department for delete:" + ResponseEntity.notFound().build());
            return ResponseEntity.notFound().build();
        }catch (InternalServerException ex){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
