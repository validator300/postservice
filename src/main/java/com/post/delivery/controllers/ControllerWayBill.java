package com.post.delivery.controllers;

import com.post.delivery.models.Message;
import com.post.delivery.models.Status;
import com.post.delivery.models.WayBill;
import com.post.delivery.repo.RepositoryMessage;
import com.post.delivery.service.WayBillService.WayBillServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/waybill")
@Slf4j
public class ControllerWayBill {
    private WayBillServiceImpl repositoryWayBill;
    private RepositoryMessage repositoryMessage;

    @Autowired
    public ControllerWayBill(WayBillServiceImpl repositoryWayBill, RepositoryMessage repositoryMessage) {
        this.repositoryWayBill = repositoryWayBill;
        this.repositoryMessage = repositoryMessage;
    }

    @GetMapping("/allWaybill")
    @PreAuthorize("hasRole('ADMIN')")
    public ModelAndView getAllWayBill() {
        log.info("find all waybill");
        List<WayBill> waybills = repositoryWayBill.getAllWayBill();
        ModelAndView modelAndView = new ModelAndView("waybill/allWaybill");
        modelAndView.addObject("waybills", waybills);
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView getWayBillForId(@PathVariable Long id) {
        log.info("get waybill for id");
        WayBill waybill = repositoryWayBill.getWayBillForId(id);
        ModelAndView modelAndView = new ModelAndView("waybill/getWaybillForId");
        modelAndView.addObject("waybill", waybill);
        return modelAndView;
    }

    @PostMapping("/save")
    public WayBill saveWayBill(@RequestBody WayBill wayBill) {
        log.info("save waybill");
        repositoryWayBill.saveWayBill(wayBill);
        repositoryMessage.save(new Message(wayBill.getId(), "Execute", Status.NEW.name()));
        return wayBill;
    }

    @GetMapping("/delete/{id}")
    public void deleteWayBillForId(@PathVariable Long id) {
        log.info("delete waybill");
        repositoryWayBill.deleteWayBillForId(id);
    }
}
