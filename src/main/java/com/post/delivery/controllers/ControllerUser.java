package com.post.delivery.controllers;

import com.post.delivery.exception.EmployeeNotFoundException;
import com.post.delivery.exception.InternalServerException;
import com.post.delivery.models.User;
import com.post.delivery.repo.RepositoryUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
@Slf4j
public class ControllerUser {

    private RepositoryUser repositoryUser;

    @Autowired
    public ControllerUser(RepositoryUser repositoryUser) {
        this.repositoryUser = repositoryUser;
    }

    @GetMapping("/allUsers")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getAllWayBill() {
        log.info("find all users");
        return repositoryUser.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Optional<User>> getUserForId(@PathVariable Long id) {
        log.info("get user for id");
        try {
            Optional<User> user = repositoryUser.findById(id);
            return ResponseEntity.ok(user);
        }
        catch (EmployeeNotFoundException ex){
            log.info("not found user for id:" + ResponseEntity.notFound().build());
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/save")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<Object> saveUser(@RequestBody User user) {
        log.info("save user");
        try {
            repositoryUser.save(user);
            Long id = user.getId();
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();
            return ResponseEntity.created(location).build();
        } catch (InternalServerException ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteUserForId(@PathVariable Long id) {
        log.info("user department method");
        try {
            User user = repositoryUser.findById(id).orElse(null);
            if (user == null) {
                log.info("not found user for delete:" + ResponseEntity.notFound().build());
                return ResponseEntity.notFound().build();
            }
            repositoryUser.delete(user);
            return ResponseEntity.ok().build();
        } catch (EmployeeNotFoundException ex){
            return ResponseEntity.notFound().build();
        }catch (InternalServerException ex){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error user delete");
        }
    }
}
