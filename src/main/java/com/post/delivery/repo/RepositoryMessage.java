package com.post.delivery.repo;

import com.post.delivery.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryMessage extends JpaRepository<Message, Long> {
}
