package com.post.delivery.repo;

import com.post.delivery.models.WayBill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositoryWayBill extends JpaRepository<WayBill, Long> {
    @Query("select b from WayBill b where b.status = 'NEW'")
    List<WayBill> packagesWithStatusNew();
}
