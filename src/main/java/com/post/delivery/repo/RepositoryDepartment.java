package com.post.delivery.repo;

import com.post.delivery.models.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryDepartment extends JpaRepository<Department, Long> {
}
