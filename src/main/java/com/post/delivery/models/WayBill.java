package com.post.delivery.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Table(name = "waybill")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WayBill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "user_id")
    private Long userSender;
    @Column(name = "sender_department_id")
    private Long senderDepartment;
    @Column(name = "receiver_department_id")
    private Long receiverDepartment;
    private Long receiverPhone;
    @Column(name = "receiver_fio")
    private String receiverFIO;
    private String status;
    private LocalDateTime creationWayBill;
    private LocalDateTime updateWayBill;
}
