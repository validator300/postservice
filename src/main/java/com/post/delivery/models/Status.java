package com.post.delivery.models;

public enum Status {
    NEW,
    DELIVERED,
    EXPIRED
}
