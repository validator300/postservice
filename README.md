# Post Service
## Models
### User
- [ ] FIO
- [ ] Email
- [ ] Phone

### Department
- [ ] Identifier
- [ ] Description

### Sending a parcel
- [ ] Identifier
- [ ] User (sender)
- [ ] Recipient's department (from the directory)
- [ ] Recipient's department (from the directory)
- [ ] Recipient's phone
- [ ] Name of the recipient
- [ ] Status(New, Delivered, Expired)
- [ ] Date + time of creation
- [ ] Date+time of status change

### Notice
- [ ] Submission ID
- [ ] Text
- [ ] Status (new, sent)

## Shipping Process
1) The user creates a parcel
2) The recipient picks up or does not pick up the parcel (this is arrange by
   separately schedule, which will take every second for each
   the parcel decision whether the parcel was taken away or not. Moreover, the decision that the parcel is not
   taken 5 times more likely. After 5 seconds, if the parcel is not picked up, then it
   considered undelivered.
3) In case of transition to the final state, the notification table is added
   message to sender about successful or unsuccessful delivery
4) A separate thread periodically reads unsent messages and
   sends (outputs to a file), then sets the sending status

## Demo program execution script:
1) Register user
2) Create multiple branches
3) Create multiple submissions
4) Wait for the transition of all submissions to the final status and wait
   performing shipments

## Tools
1) spring boot
2) lombok
3) logger
4) postgresql
5) postman
6) swagger